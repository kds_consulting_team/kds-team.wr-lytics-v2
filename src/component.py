'''
Template Component main class.

'''
import csv
import functools
import glob
import gzip
import json
import logging
import os
import re
import shutil
import sys
from enum import Enum
from pathlib import Path
from typing import List

import requests
from kbc.env_handler import KBCEnvHandler
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

LYTICS_API_URL = 'https://api.lytics.io'
# configuration variables
KEY_USER_PARS = 'user_parameters'

KEY_PATH = 'path'
KEY_MODE = 'mode'
KEY_METHOD = 'method'

# additional request params
KEY_HEADERS = 'headers'
KEY_ADDITIONAL_PARS = 'additional_requests_pars'
STATUS_FORCELIST = (500, 501, 502, 503)
MAX_RETRIES = 3

# DYNAMIC PARS

KEY_TOKEN = '#token'
KEY_STREAM = 'stream'
KEY_MODEL_TYPE = 'model_type'

KEY_LOAD_OPTIONS = 'loading_options'
KEY_LOAD_TYPE = 'load_type'
KEY_DRYRUN = 'dryrun'
KEY_TIMESTAMP_FIELD = 'timestamp_field'

KEY_LQL_BLOCKS = 'lql_blocks'
KEY_NAME = 'name'
KEY_MODEL_MAPPING = 'model_mapping'
KEY_SRC_COL = 'src_col'
KEY_DST_NAME = 'dst_name'
KEY_SHORT_DESC = 'short_desc'
KEY_TYPE = 'type'
KEY_IS_BY = 'is_by'

KEY_RAW_QUERY = 'raw_query'

# #### Keep for debug
KEY_DEBUG = 'debug'

MANDATORY_PARS = [KEY_TOKEN, KEY_STREAM]
MANDATORY_IMAGE_PARS = []

APP_VERSION = '0.0.1'


class LqlValidationException(Exception):
    """
    Throw on validation error.
    """

    def __init__(self, message, full_message):
        message = message
        full_message = full_message


class LyticsTypes(Enum):
    INT = "INT",
    NUMBER = "NUMBER",
    STRING = "STRING",
    DATE = "DATE",
    EMAIL = "EMAIL",
    EMAIL_DOMAIN = "EMAIL_DOMAIN",
    CUSTOM_LQL = "CUSTOM_LQL"


class LoadTypes(Enum):
    UPDATE_MODEL_AND_DATA = "UPDATE_MODEL_AND_DATA",
    MODEL_ONLY = "MODEL_ONLY",
    DATA_ONLY = "DATA_ONLY"

    @classmethod
    def list(cls):
        return list(map(lambda c: c.name, cls))

    @classmethod
    def validate_fields(cls, fields: List[str]):
        errors = []
        for f in fields:
            if f not in cls.list():
                errors.append(f'"{f}" is not valid {cls.__name__} value!')
        if errors:
            raise ValueError(
                ', '.join(errors) + f'\n Supported {cls.__name__} values are: [{cls.list()}]')


def response_error_handling(func):
    """Function, that handles response handling of HTTP requests.
    """

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try:
            r = func(*args, **kwargs)
            r.raise_for_status()
        except requests.HTTPError as e:
            if e.response.status_code in [401, 403]:
                raise Exception(f"{r.json()['message']}. Please check your API token and permissions!")
            elif r.status_code > 299:
                raise Exception(f"Failed to send request with error {r.json()['message']}")
        return r

    return wrapper


class Component(KBCEnvHandler):

    def __init__(self, debug=False):
        # for easier local project setup
        default_data_dir = Path(__file__).resolve().parent.parent.joinpath('data').as_posix() \
            if not os.environ.get('KBC_DATADIR') else None

        KBCEnvHandler.__init__(self, MANDATORY_PARS, log_level=logging.DEBUG if debug else logging.INFO,
                               data_path=default_data_dir)
        # override debug from config
        if self.cfg_params.get(KEY_DEBUG):
            debug = True
        if debug:
            logging.getLogger().setLevel(logging.DEBUG)
        logging.info('Running version %s', APP_VERSION)
        logging.info('Loading configuration...')

        try:
            self.validate_config(MANDATORY_PARS)
        except ValueError as e:
            logging.exception(e)
            exit(1)
        # validate stream name
        stream = self.cfg_params[KEY_STREAM]
        if not self._test_lql_name_valid(stream):
            raise ValueError(
                f'Invalid stream name "{stream}". Stream name cannot contain empty characters!')
        # intialize instance parameteres
        # set constants
        self.cfg_params[KEY_PATH] = 'https://bulk.lytics.io/collect/bulk/' + self.cfg_params[KEY_STREAM]
        self.cfg_params[KEY_METHOD] = 'POST'

        timestamp_field = self.cfg_params.get(KEY_TIMESTAMP_FIELD, None)

        self.cfg_params[KEY_ADDITIONAL_PARS] = [
            {
                "key": "params",
                "value": {
                    "dryrun": self.cfg_params.get(KEY_DRYRUN, False),
                    "filename": stream + '.csv',
                    "timestamp_field": timestamp_field
                }
            }
        ]
        self.cfg_params[KEY_HEADERS] = [{
            "key": "Authorization",
            "value": self.cfg_params[KEY_TOKEN]
        }, {
            "key": "Content-Type",
            "value": "application/csv"
        }]

        self.dry_run = self.cfg_params[KEY_LOAD_OPTIONS].get(KEY_DRYRUN)
        self.load_type = self.cfg_params[KEY_LOAD_OPTIONS].get(KEY_LOAD_TYPE, LoadTypes.UPDATE_MODEL_AND_DATA.name)
        self._auth_token = self.cfg_params[KEY_TOKEN]

        LoadTypes.validate_fields([self.load_type])

    def run(self):
        '''
        Main execution code
        '''
        params = self.cfg_params  # noqa

        logging.info('Processing input mapping.')
        in_tables = glob.glob(self.tables_in_path + "/*[!.manifest]")
        if len(in_tables) == 0:
            logging.exception('There is no table specified on the input mapping! You must provide one input table!')
            exit(1)
        elif len(in_tables) > 1:
            logging.error(
                'There is more than one table specified on the input mapping! You must provide single input table!!')
            exit(1)

        in_table = in_tables[0]

        logging.info("Building parameters..")
        # build headers
        headers = self._build_headers()
        # build additional parameters
        additional_params = self._build_request_parameters()
        additional_params['headers'] = headers

        if self.dry_run:
            logging.warning("Running in DRY RUN mode.")

        logging.info(f"Running in Load Mode: {self.load_type}")

        if self.load_type != LoadTypes.DATA_ONLY.name:
            logging.info("Processing model...")
            query_path = self._process_lql(in_table)

            if not self.dry_run:
                try:
                    logging.info("Upserting LQL query into Lytics.")
                    self.upload_lql_query(query_path)
                except Exception as e:
                    logging.error(f'Failed to send the query {e}')
                    exit(1)

        if self.load_type in (LoadTypes.DATA_ONLY.name, LoadTypes.UPDATE_MODEL_AND_DATA.name):
            logging.info(f"Pushing data to the stream: {params[KEY_STREAM]}")
            self.send_binary_data(params[KEY_PATH], additional_params, in_table, compress=True)

    logging.info("Writer finished")

    def query_test_evaluation(self, query_path, in_table, dry_run=False):
        # build sample data
        sample_data = {}
        with open(in_table, mode='r') as input_m:
            reader = csv.DictReader(input_m)
            sample_data = [line for i, line in enumerate(reader) if i <= 5]

        headers = self._get_auth_header()
        headers['Content-Type'] = 'text/plain'
        for line in sample_data:
            params = {'headers': headers, "params": line}
            res = self.send_binary_data(LYTICS_API_URL + '/api/query/_test', params, query_path)
            if dry_run:
                logging.warning(json.dumps(res.json()["data"]))

    def upload_lql_query(self, query_path):
        headers = self._get_auth_header()
        headers['Content-Type'] = 'text/plain'
        params = {'headers': headers}
        self.send_binary_data(LYTICS_API_URL + '/api/query', params, query_path)

    def _build_headers(self):
        headers = {}
        for h in self.cfg_params[KEY_HEADERS]:
            headers[h["key"]] = h["value"]
        return headers

    def _build_request_parameters(self):
        additional_params = {}
        for h in self.cfg_params[KEY_ADDITIONAL_PARS]:
            # convert boolean
            val = h["value"]
            if isinstance(val, str) and val.lower() in ['false', 'true']:
                val = val.lower() in ['true']
            additional_params[h["key"]] = val
        return additional_params

    def _get_auth_header(self):
        return {'Authorization': self._auth_token}

    @response_error_handling
    def send_request(self, url, additional_params, method='POST'):
        s = requests.Session()

        r = self._requests_retry_session(session=s).request(method, url, **additional_params)

        return r

    def send_binary_data(self, url, additional_request_params, in_table, compress=False):
        in_path = in_table
        if compress:
            in_path = in_path + '.gz'
            with gzip.open(in_path, 'wb') as f_out:
                shutil.copyfileobj(open(in_table, mode='rb'), f_out)

        with open(in_path, mode='rb') as in_file:
            additional_request_params['data'] = in_file
            return self.send_request(url, additional_request_params)

    def _requests_retry_session(self, session=None):
        session = session or requests.Session()
        retry = Retry(
            total=MAX_RETRIES,
            read=MAX_RETRIES,
            connect=MAX_RETRIES,
            backoff_factor=0.5,
            status_forcelist=STATUS_FORCELIST,
            allowed_methods=('GET', 'POST', 'PATCH', 'UPDATE')
        )
        adapter = HTTPAdapter(max_retries=retry)
        session.mount('http://', adapter)
        session.mount('https://', adapter)
        return session

    def _process_lql(self, in_table):
        params = self.cfg_params
        if params.get(KEY_RAW_QUERY):
            # hacker mode
            logging.info("Running in hacker mode. Full query definition is being used.")
            query = params.get(KEY_RAW_QUERY)
        else:
            logging.info("Building LQL query from blocks.")
            query = self.build_lql(in_table)

        if query:
            query_path = os.path.join(self.files_in_path, 'query.lql')
            with open(query_path, 'wt') as out:
                out.write(query)

            # test query
            try:
                logging.warning("Running dry run of the LQL / data upload.",
                                extra={"full_message": json.dumps(query)})
                self.query_test_evaluation(query_path, in_table, self.dry_run)
            except Exception as e:
                if "Invalid test" in str(e):
                    error = self._process_evaluation_error(str(e), query)
                    logging.error(f'{error}. See log detail for full query. ',
                                  extra={"full_message": json.dumps(query)})
                else:
                    logging.error(str(e), extra={"full_message": json.dumps(query)})

                exit(1)
        return query_path

    def build_lql(self, in_table):
        # validate model
        lql_blocks = self.cfg_params.get(KEY_LQL_BLOCKS, [])
        self._validate_lql_blocks(in_table, lql_blocks)

        model_type = self.cfg_params[KEY_MODEL_TYPE]
        query = self._build_query_from_blocks(lql_blocks, self.cfg_params[KEY_STREAM], model_type)
        return query

    def _validate_lql_blocks(self, in_table, lql_blocks):
        errmsgs = list()
        for block in lql_blocks:
            msg = self._validate_model_mapping(in_table, block.get(KEY_MODEL_MAPPING))
            errmsgs.extend(msg)

        if errmsgs:
            raise ValueError(f'Input mapping validation failed: {",".join(errmsgs)}')

    def _validate_model_mapping(self, in_table, model_mapping):
        with open(in_table, mode='r') as input_m:
            reader = csv.DictReader(input_m)
            header = reader.fieldnames
            missing_cols = []
            err_name_cols = []
            for m in model_mapping:
                if m[KEY_TYPE] != LyticsTypes.CUSTOM_LQL.name and m[KEY_SRC_COL] not in header:
                    missing_cols.append(m[KEY_SRC_COL])
                if not self._test_lql_name_valid(m[KEY_DST_NAME]):
                    err_name_cols.append(m[KEY_DST_NAME])

        errmsgs = []
        if missing_cols:
            errmsgs.append(f'Some source columns specified in mapping do not exist in the source! {missing_cols}')
        if err_name_cols:
            errmsgs.append(
                f'Some destination column names contain invalid characters! Check for any whitespaces! {err_name_cols}')

        return errmsgs

    def _test_lql_name_valid(self, name):
        return not bool(re.search(r'\s', name))

    def _build_query_from_blocks(self, lql_blocks, stream, model_type):
        query_lines = []
        by_columns = set()
        query = 'SELECT'
        query_lines.append(query)
        select_lines = []
        nr_blocks = len(lql_blocks)
        for idx, block in enumerate(lql_blocks):
            model_mapping = block[KEY_MODEL_MAPPING]
            block_name = block[KEY_NAME]
            # add separator
            select_lines.append(self._create_block_separator(block_name))

            # build query
            block_lines, curr_by_columns = self._build_lql_block(model_mapping)
            if idx < nr_blocks - 1:
                block_lines += ',\n'
            select_lines.append(block_lines)

            by_columns.update(curr_by_columns)

        # build body
        select_lines_str = '\n'.join(select_lines)
        query_lines.append(select_lines_str)
        from_cl = f"\n\nFROM {stream}\n" \
                  f"  INTO {model_type} BY {' OR '.join(by_columns)}\n" \
                  f"  ALIAS {stream}"
        query_lines.append(from_cl)

        return self._format_lql(query_lines)

    def _create_block_separator(self, block_name):
        return f'\n-- ------------------------------- {block_name.upper()} -------------------------------\n'

    def _build_lql_block(self, model_mapping):
        """

        :param model_mapping:
        :return:
        """
        query_line_tokens = []
        query_lines = []
        # at this point I know it exists
        by_columns = []
        for ix, m in enumerate(model_mapping):
            if m[KEY_IS_BY]:
                by_columns.append(m[KEY_DST_NAME])

            source = m[KEY_SRC_COL]
            data_type = m[KEY_TYPE]
            kind = ''
            # custom LQL
            if data_type == LyticsTypes.CUSTOM_LQL.name:
                # extract type from src
                kind_spl = source.lower().split(' kind ')
                if len(kind_spl) > 1:
                    data_type = kind_spl[1]
                else:
                    data_type = None
                source = source[0:len(kind_spl[0])]

            if data_type:
                kind = f'KIND {data_type}'

            if data_type == LyticsTypes.EMAIL.name:
                source = f'email({m[KEY_SRC_COL]})'
                kind = ''

            if data_type == LyticsTypes.EMAIL_DOMAIN.name:
                source = f'emaildomain({m[KEY_SRC_COL]})'
                kind = ''

            line_tokens = [source, 'AS', m[KEY_DST_NAME], 'SHORTDESC', f'"{m[KEY_SHORT_DESC]}"', kind]
            query_line_tokens.append(line_tokens)

        query_lines = self._align_ident(query_line_tokens)
        query_lines_str = ',\n'.join(query_lines)
        return query_lines_str, by_columns

    def _align_ident(self, token_lists):
        largest_idents = {}
        block_lines = []
        lists_w_empty_lines = []
        # collect largest idents and add empty chars
        for tlist in token_lists:
            new_list = tlist.copy()
            prev_idx = 0
            for idx, t in enumerate(tlist):
                size = len(t)
                if size > largest_idents.get(idx, 0):
                    largest_idents[idx] = size
                new_list.insert(prev_idx + idx + 1, '')
                prev_idx += 1
            lists_w_empty_lines.append(new_list)

        # build block string
        for tlist in lists_w_empty_lines:
            prev_idx = 0
            for idx in range(int((len(tlist) - 1) / 2)):
                ident = (largest_idents.get(idx, 1) - len(tlist[prev_idx + idx])) + 2
                tlist[prev_idx + idx + 1] = ' '.join(['' for i in range(ident)])
                prev_idx += 1

            line = ''.join(tlist)
            block_lines.append(line.strip())

        return block_lines

    def _format_lql(self, query_lines):
        query = '\n'.join(query_lines)
        return query

    def _process_evaluation_error(self, msg, query):
        error_msg = msg.split("error", 1)[1]
        error_near = ''
        if re.search(r'Line:(\d+)', error_msg):
            line_nr = re.search(r'Line:(\d+)', error_msg)[0].split(':')[1]
            col_nr = re.search(r'Col:(\d+)', error_msg)[0].split(':')[1]
            error_near = query.split('\n')[int(line_nr) - 1][int(col_nr) - 5:int(col_nr) + 5]

        return f'{error_msg}, near: "{error_near}"'


"""
        Main entrypoint
"""
if __name__ == "__main__":
    if len(sys.argv) > 1:
        debug_arg = sys.argv[1]
    else:
        debug_arg = False
    try:
        comp = Component(debug_arg)
        comp.run()
    except requests.HTTPError as er:
        if er.response.status_code == 401:
            er.args = (er.args[0] + ' Check your API key!',)
        logging.exception(er)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(1)
