A **single table** on the input must be specified.

- The data is sent as gzipped csv via the [bulk upload API](https://learn.lytics.com/documentation/developer/api-docs/data-upload#bulk-csv-upload-bulk-csv-upload-post) 
so large volumes are supported. 
- The event stream is automatically created if does not exist.
- Creates / Updates a LQL query based on parameters with alias matching the Stream Name.
- May be executed multiple times

### Dry run

Dry run to verify the data. Only do a dry-run to see if the data is formatted correctly and the query is structured properly. 

**NOTE:**  The data upload and query dry run is executed regardless the `Model -> Load Type` setup. 
When in dry run mode a sample data row is sent to the `query/test` endpoint and the result printed in the 
job log to verify it behaves properly. The job log will contain record like this:

```
Running dry run of the LQL / data upload.
{"_created": "2020-09-22T13:33:43.741754223Z", "_modified": "2020-09-22T13:33:43.741754223Z", "email": "david@keboola.com", "last_order_date": "2020-01-01T00:00:00Z", "name": "keboola.com", "order_value": "150", "test": "a"}
```
Moreover, the generated LQL query will be available in the log detail after clicking on `Running dry run of the LQL / data upload.` event.

## LQL blocks

Model mapping that will generate the LQL query. It is structured into separate blocks, that are visually separated for better readability. 
It is recommended to keep the first block `Identifiers` containing only identifier columns.

Query with the same alias as the `Stream name` will be created or updated according to the specified mapping.

No model is created if the mapping is empty.


### Block column definition

Mapping of source column tables to the destination table columns

- **Column** - Name of the input table column
- **Column name** - Destination (LQL) column name. Must not contain blank characters
- **Description** - Column description
- **Type** - if set to `Email`, `email()` function will be applied to the field
- **Is Identifier** - If set to true, the field will be used in the `BY` clause. May be set only on one column.

![lql block](docs/imgs/lql_block.png)

#### Data Types

Apart from standard primitive datatypes, additional are supported:

- **EMAIL** - `email()` function will be applied to the field
- **EMAIL_DOMAIN** - `emaildomain()` function will be applied to the field

##### **CUSTOM_LQL**

When `CUSTOM_LQL` datatype is selected you may specify custom LQL (function) in the `Column` field 
instead of the source table column name. This allows to apply some
 additional LQL functions such as `Map` datatypes. 
 The name of the actual existing column from the source table must be included.
 It is possible to define the KIND as well.
 
 **Examples**:
 
 `map(key1, todate(date_field)) KIND map[string]time` - where `date_field` is a column in the input table
 `domain(url)`- where `url` is a column in the input table



## Raw query (hacker mode)

Optionally you may choose to specify the whole LQL query manually. In such case you may paste your LQL code
into the `Raw query field` and all LQL blocks will be overridden. Also the `Stream name` will take no effect. 

**NOTE** Be sure to refer to existing column names in the input table.