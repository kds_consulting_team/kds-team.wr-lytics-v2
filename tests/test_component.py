'''
Created on 12. 11. 2018

@author: esner
'''
import mock
import os
import unittest
from freezegun import freeze_time

from component import Component


class TestComponent(unittest.TestCase):

    def setUp(self) -> None:
        path = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                            'data_test', 'data1')
        os.environ["KBC_DATADIR"] = path
        self.data_test_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                          'data_test')

    # set global time to 2010-10-10 - affects functions like datetime.now()
    @freeze_time("2010-10-10")
    # set KBC_DATADIR env to non-existing dir
    @mock.patch.dict(os.environ, {'KBC_DATADIR': './non-existing-dir'})
    def test_run_no_cfg_fails(self):
        with self.assertRaises(ValueError):
            comp = Component()
            comp.run()

    def test_invalid_stream_fails(self):
        os.environ["KBC_DATADIR"] = os.path.join(self.data_test_dir, 'data_invalid_stream')
        with self.assertRaises(ValueError):
            comp = Component()

    def test_invalid_dest_col_fails(self):
        os.environ["KBC_DATADIR"] = os.path.join(self.data_test_dir, 'data_invalid_dest_col')
        with self.assertRaises(ValueError):
            comp = Component()
            comp.run()

    def test_build_lql_valid(self):
        os.environ["KBC_DATADIR"] = os.path.join(self.data_test_dir, 'data1')

        comp = Component()
        with open(os.path.join(comp.files_in_path, 'query.lql')) as f:
            valid_lql = f.read()
        gen = comp.build_lql(os.path.join(comp.tables_in_path, 'test.csv'))
        self.assertEqual(valid_lql, gen)


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
