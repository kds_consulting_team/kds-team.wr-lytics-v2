# KBC Lytics Writer

Write data to custom Lytics streams. Allows sending large volumes of data using gzipped stream and dry run mode 
to verify your data.

**Table of contents:**  
  
[TOC]


# Functionality notes

Send events to your custom event stream. Takes a **single table** on the input, containing the data.

- The data is sent as gzipped csv via the [bulk upload API](https://learn.lytics.com/documentation/developer/api-docs/data-upload#bulk-csv-upload-bulk-csv-upload-post) 
so large volumes are supported. 
- The event stream is automatically created if does not exist.
- Creates a LQL query based on parameters with alias matching the Stream Name.
- May be executed multiple times


Each row represents a **single stream**. The query is split into **blocks** for better readability. 

# Configuration

## Authorization

**API Token:** Your Lytics API token

## Stream configuration 

Each row represents a **single stream**. The query is split into **blocks** for better readability. 


## Stream

Name of the event stream. Note that ifn the event stream does not exist already, it is created automatically.

## Model Type

Type of the table you want to update. By default `user`, but you may want to update also `content` or other.


## Loading Options

![Loding Opts](docs/imgs/loading_opts.png)



### Load Type

The load type defines what actions will be performed on each exectuion:

- **Update Model and Data** - uploads model (LQL query) and loads data each time
- **Update Model Only** - Only updates the LQL query
- **Update Data Only** - Only loads data to the data stream, the LQL query stays intact

### Dry run

Dry run to verify the data. Only do a dry-run to see if the data is formatted correctly and the query is structured properly. 

**NOTE:**  The data upload and query dry run is executed regardless the `Model -> Load Type` setup. 
When in dry run mode a sample data row is sent to the `query/test` endpoint and the result printed in the 
job log to verify it behaves properly. The job log will contain record like this:

```
Running dry run of the LQL / data upload.
{"_created": "2020-09-22T13:33:43.741754223Z", "_modified": "2020-09-22T13:33:43.741754223Z", "email": "david@keboola.com", "last_order_date": "2020-01-01T00:00:00Z", "name": "keboola.com", "order_value": "150", "test": "a"}
```

Moreover, the generated LQL query will be available in the log detail after clicking on `Running dry run of the LQL / data upload.` event:

![Dry](docs/imgs/dry_run_lql.png)

The LQL itself is available under the `_full_message` attribute:

![Dry lql](docs/imgs/sample_lql.png)


### Timestamp Field

Optional parameter. The name of the column or field in file that contains event timestamp.



## LQL blocks

Model mapping that will generate the LQL query. It is structured into separate blocks, that are visually separated for better readability. 
It is recommended to keep the first block `Identifiers` containing only identifier columns.

Example LQL output:

```
SELECT

-- ------------------------------- IDENTIFIERS -------------------------------

email(email) AS email SHORTDESC "Email Address",


-- ------------------------------- SUBSCRIPTIONS -------------------------------

name  AS subtype                 SHORTDESC "Subscription Type"       KIND STRING,
date  AS subscription_start_date SHORTDESC "Subscription Start Date" KIND DATE,
price AS subscription_end_date   SHORTDESC "Subscription End Date"   KIND NUMBER


FROM custom-keboola-testing-model
  INTO user BY email
  ALIAS custom-keboola-testing-model
```

Query with the same alias as the `Stream name` will be created or updated according to the specified mapping.

No model is created if the mapping is empty.


### Block column definition

Mapping of source column tables to the destination table columns

- **Column** - Name of the input table column
- **Column name** - Destination (LQL) column name. Must not contain blank characters
- **Description** - Column description
- **Type** - if set to `Email`, `email()` function will be applied to the field
- **Is Identifier** - If set to true, the field will be used in the `BY` clause. May be set only on one column.

![lql block](docs/imgs/lql_block.png)

#### Data Types

Apart from standard primitive datatypes, additional are supported:

- **EMAIL** - `email()` function will be applied to the field
- **EMAIL_DOMAIN** - `emaildomain()` function will be applied to the field

##### **CUSTOM_LQL**

When `CUSTOM_LQL` datatype is selected you may specify custom LQL (function) in the `Column` field 
instead of the source table column name. This allows to apply some
 additional LQL functions such as `Map` datatypes. 
 The name of the actual existing column from the source table must be included.
 It is possible to define the KIND as well.
 
 **Examples**:
 
 `map(key1, todate(date_field)) KIND map[string]time` - where `date_field` is a column in the input table
 `domain(url)`- where `url` is a column in the input table



## Raw query (hacker mode)

Optionally you may choose to specify the whole LQL query manually. In such case you may paste your LQL code
into the `Raw query field` and all LQL blocks will be overridden. Also the `Stream name` will take no effect. 

**NOTE** Be sure to refer to existing column names in the input table.

![Raw query](docs/imgs/raw_query.png) 


# Development

If required, change local data folder (the `CUSTOM_FOLDER` placeholder) path to your custom path in the docker-compose file:

```yaml
    volumes:
      - ./:/code
      - ./CUSTOM_FOLDER:/data
```

Clone this repository, init the workspace and run the component with following command:

```
git clone repo_path my-new-component
cd my-new-component
docker-compose build
docker-compose run --rm dev
```

Run the test suite and lint check using this command:

```
docker-compose run --rm test
```

# Integration

For information about deployment and integration with KBC, please refer to the [deployment section of developers documentation](https://developers.keboola.com/extend/component/deployment/) 